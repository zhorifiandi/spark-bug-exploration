
echo "Start killing SparkSubmit"
jps | grep SparkSubmit | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
    # your code goes here
done
echo "Finish killing SparkSubmit"

echo "Start killing Worker"
jps | grep Worker | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
    # your code goes here
done
echo "Finish killing Worker"

echo ""

echo "Start killing Master"
jps | grep Master | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
    # your code goes here
done
echo "Finish killing Master"

# while [ 1=1 ]
# do
# 	say -v Alex "taw long punkgil doneniyar! sai-ya, su dah sel lhe sai." -r 170
# 	sleep 3
# done