if [ "$1" != "" ]; then
    if [ "$1" == 0 ]; then
    	echo "Getting Master Node.."
    	awk '/Started daemon with process name:/{print $10}' "/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-Bug-Exploration/DMCK-master/spark/logs/spark-zhorifiandi-org.apache.spark.deploy.master.Master-1-Zhorifiandi.local.out"

    else
    	awk '/Started daemon with process name:/{print $10}' "/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-Bug-Exploration/DMCK-master/spark/logs/spark-zhorifiandi-org.apache.spark.deploy.worker.Worker-$1-Zhorifiandi.local.out"
    fi
else
    echo "How to run:"
    echo ">> ./startNode.sh [WORKER_NUMBER]"
    echo ">> For master, WORKER_NUMBER=0"
    return
fi