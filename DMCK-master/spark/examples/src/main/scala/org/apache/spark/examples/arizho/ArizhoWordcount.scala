/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// scalastyle:off println
package org.apache.spark.examples.arizho


import org.apache.spark._

import java.io.File;

/**
 * Counts words in new text files created in the given directory
 * Usage: HdfsWordCount <directory>
 *   <directory> is the directory that Spark Streaming will use to find and read new text files.
 *
 * To run this on your local machine on directory `localdir`, run this example
 *    $ bin/run-example \
 *       org.apache.spark.examples.streaming.HdfsWordCount localdir
 *
 * Then create a text file in `localdir` and the words in the file will get counted.
 */
object ArizhoWordcount {
  def main(args: Array[String]) {
    if (args.length < 1) {
      System.err.println("Usage: ArizhoWordcount <directory>")
      System.exit(1)
    }

    val sparkConf = new SparkConf().setAppName("ArizhoWordcount").set("spark.eventLog.enabled","true").set("spark.eventLog.dir","arizho")
    val sc = new SparkContext(sparkConf)

    // Create the FileInputDStream on the directory and use the
    // stream to count words in new files created
    val lines = sc.textFile(args(0))
    
    val words = lines.flatMap(_.split("\\s+")) 

    val pre_wc = words.map(w => (w, 1))

    print("\n\n\nMAP DONE\n\n\n")

    pre_wc.persist()

    print("\n\nBlocking, wait register bm exec, then make worker timeout\n\n")


    var checkFile = true
    while (checkFile){
        val file = new File("/tmp/ipc/manual/registered_bm_exec_1")
        if (file.exists()){
            Runtime.getRuntime().exec("rm /tmp/ipc/manual/registered_bm_exec_1").waitFor();
            print("\n\nDRIVER: BREAK FROM BLOCKINGQ!!!\n\n")
            checkFile = false
        }
        Thread.sleep(50)
    }

    checkFile = true
    while (checkFile){
        val file = new File("/tmp/ipc/manual/registered_bm_exec_0")
        if (file.exists()){
            Runtime.getRuntime().exec("rm /tmp/ipc/manual/registered_bm_exec_0").waitFor();
            print("\n\nDRIVER: BREAK FROM BLOCKINGQ!!!\n\n")
            checkFile = false
        }
        Thread.sleep(50)
    }

    print("\n\nBreaker! make worker timeout\nNOW, Let DMCK DOOO THIS!!\n")
    // NOW, Let DMCK DO THIS!!
    // Runtime.getRuntime().exec("touch /tmp/ipc/ack/timeout-1").waitFor();

    print("\n\nWaiting remove worker\n\n")

    var checkFile2 = true
    while (checkFile2){
        val file = new File("/tmp/ipc/manual/kill-executor1")
        if (file.exists()){
            Runtime.getRuntime().exec("rm /tmp/ipc/manual/kill-executor1").waitFor();
            print("\n\nDRIVER: BREAK FROM BLOCKING KILL!!!\n\n")
            checkFile2 = false
        }
        Thread.sleep(50)
    }

    print("\n\nTask: ARIZHO: Killing Executor 1! \n\n")
    sc.killExecutors(Seq("1"))
    Runtime.getRuntime().exec("touch /tmp/ipc/manual/already_kill_exec1").waitFor();


    val wc = pre_wc.reduceByKey(_ + _)
    wc.saveAsTextFile("README.count")  

    pre_wc.unpersist()
  }
}
// scalastyle:on println
