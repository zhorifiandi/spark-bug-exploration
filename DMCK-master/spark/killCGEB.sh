touch /tmp/ipc/manual/kodokgoreng
echo "Start killing CoarseGrainedExecutorBackend"
jps | grep CoarseGrainedExecutorBackend | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
done
echo "Finish killing CoarseGrainedExecutorBackend"