echo "Start killing SparkSubmit"
jps | grep SparkSubmit | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -2 $line
    kill -9 $line
    # your code goes here
done
echo "Finish killing SparkSubmit"