val lines = sc.textFile(args(0))

val words = lines.flatMap(_.split("\\s+")) 

words.persist()

print("\n\nSleeping for 210 sec\n\n")

Thread.sleep(210000)

print("\n\nDone Sleeping, Killing executor 1\n\n")
sc.killExecutors(Seq("1"))

val wc = words.map(w => (w, 1)).reduceByKey(_ + _)
wc.saveAsTextFile("README.count")  

words.unpersist()