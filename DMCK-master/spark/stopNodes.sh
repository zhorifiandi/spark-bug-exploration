echo "Start killing SparkSubmit"
jps | grep SparkSubmit | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -2 $line
    kill -9 $line
    # your code goes here
done
echo "Finish killing SparkSubmit"

echo "Start killing Worker"
jps | grep Worker | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
done
echo "Finish killing Worker"

echo ""

echo "Start killing Master"
jps | grep Master | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
    # your code goes here
done
echo "Finish killing Master"

echo "Start killing CoarseGrainedExecutorBackend"
jps | grep CoarseGrainedExecutorBackend | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
done
echo "Finish killing CoarseGrainedExecutorBackend"

echo "Start killing Main"
jps | grep Main | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
done
echo "Finish killing Main"

echo "Start killing Elasticsearch"
jps | grep Elasticsearch | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
done
echo "Finish killing Elasticsearch"

echo "Start killing Nailgun"
jps | grep Nailgun | grep -Eo '[0-9]{1,7}' | while read -r line ; do
    echo "Killing $line"
    kill -9 $line
done
echo "Finish killing Nailgun"

# echo "Start killing All"
# jps | grep -Eo '[0-9]{1,7}' | while read -r line ; do
#     echo "Killing $line"
#     kill -9 $line
#     # your code goes here
# done
# echo "Finish killing All"

