/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.samc
import org.apache.spark.Logging
import java.io._

import scala.io.Source
/** eventTypeId :
  1.
  */
class EventInterceptor( eventTypeId : Int, // same
                        eventType : String, // same
                        // stageId : Int,
                        sendNode : Int,
                        recvNode : Int,
                        nodeState : Int,
                        payload : String
                      ) extends Logging {

  var eventId: Int = getEventId
  var ipcDir : String = "/tmp/ipc"
  var fileName: String = "no file name"
  var samcResponse = false

  // HARUSNYA YANG INI
  def submitAndWait(): Unit = { // begin to write /new/filename
    logWarning(s"ARIZHO CHANGED >>>> Real DMCK ${eventType} : ${payload}")
    this.fileName = createFileName
    writeNewEvent()
    commitEvent()
    waitAck()
  }

   // def submitAndWait(): Unit = { // begin to write /new/filename
   //   logWarning(s"MANUAL DMCK ${eventType} : ${payload}")
   //   this.fileName = createFileName
   //   writeNewEvent()
   // }

  //HARUSNYA YANG INI
  // def updateState(): Unit = { // begin to write /new/filename
  //   this.fileName = createFileNameNodeUpdate
  //   writeNewUpdate()
  //   commitEvent()
  // }

  def updateState(): Unit = { // begin to write /new/filename
    this.fileName = createFileNameNodeUpdate
    writeNewUpdate()
    commitEvent()
  }

  private def writeNewUpdate(): Unit = {
    try {
           logInfo("samc: write file at " + ipcDir + "/new/" + fileName)
      val writer = new PrintWriter(ipcDir + "/new/" + createFileNameNodeUpdate, "UTF-8")
      writer.print("sendNode=" + sendNode + "\n")
      writer.print("recvNode=" + recvNode + "\n")
      writer.print("nodeState=" + nodeState + "\n")
      writer.print("eventTypeId=" + eventTypeId + "\n")
      writer.print("eventType=" + eventType + "\n")
      writer.print("payload=" + payload + "\n")
      // writer.print("stageId=" + stageId + "\n")
      writer.print("eventId=" + eventId)
      writer.close()
    } catch {
      case e: Exception =>
        logError("samc: Error on writing new event file", e)
    }
  }

  private def writeNewEvent(): Unit = {
    try {
     logInfo("samc: write file at " + ipcDir + "/new/" + fileName)
      val writer = new PrintWriter(ipcDir + "/new/" + fileName, "UTF-8")
      writer.print("sendNode=" + sendNode + "\n")
      writer.print("recvNode=" + recvNode + "\n")
      writer.print("nodeState=" + nodeState + "\n")
      writer.print("eventTypeId=" + eventTypeId + "\n")
      writer.print("eventType=" + eventType + "\n")
      writer.print("payload=" + payload + "\n")
      // writer.print("stageId=" + stageId + "\n")
      writer.print("eventId=" + eventId)
      writer.close()
    } catch {
      case e: Exception =>
         logError("samc: Error on writing new event file", e)
    }
  }

  private def commitEvent(): Unit = {
   logInfo("samc: Writing event file to /new/ dir by executing : "+"mv " + ipcDir + "/new/" +
     fileName + " " + ipcDir + "/send/" + fileName)
    try
      Runtime.getRuntime.exec("mv " + ipcDir + "/new/" + fileName + " " + ipcDir + "/send/" + fileName)
    catch {
      case e: Exception =>
        logError("samc: Error in committing file.")
    }
  }

  private def waitAck() = {
    val ackFileName = ipcDir + "/ack/" + fileName
    while (!new java.io.File(ackFileName).exists) {
//      logInfo("samc: Waiting ack file. "+ ackFileName)
      try
        Thread.sleep(20)
      catch {
        case ie: InterruptedException =>
          ie.printStackTrace()
      }
    }

    logInfo("samc: got ack file. "+ ackFileName)

    if (new java.io.File(ackFileName).exists) {
      try
          for (line <- Source.fromFile(ackFileName).getLines) {
            if (line.contains("execute=true")) samcResponse = true
          }
      catch {
        case e: Exception =>
          logError("samc: Error in reading ack file")
      }

      try
        Runtime.getRuntime.exec("rm " + ackFileName)
      catch {
        case e: Exception =>
          logError("samc: Error in deleting ack file.")
      }
    }
//    logInfo("samc: samcResponse = " + samcResponse)
    samcResponse
  }

//  def getFileDir: Ordering.String.type = fileDir

  def createFileName: String = {
    val time = System.nanoTime
    val postfix = time % 1000000
    val filename = "spark-" + String.valueOf(eventId) + "-" + String.valueOf(postfix)
    filename
  }

  def createFileNameNodeUpdate: String = {
    val time = System.nanoTime
    val postfix = time % 1000000
    val filename = "sparkUpdate-" + String.valueOf(eventId) + "-" + String.valueOf(postfix)
    filename
  }

  def getEventId: Int = {
    val prime = 19
    var hash = 1
    hash = prime * hash + sendNode
    hash = prime * hash + recvNode
    hash = prime * hash + nodeState
    hash = prime * hash + eventTypeId
    hash
  }

  def hasSAMCResponse: Boolean = samcResponse

  def printToLog(): Unit = {
//    logInfo("samc: " + fileName + "={sendNode: " + sendNode + ", recvNode: " +
//      recvNode + ", interceptEventType: " + eventType.toString + "}")
  }
 }