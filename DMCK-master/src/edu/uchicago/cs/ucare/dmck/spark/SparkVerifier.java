package edu.uchicago.cs.ucare.dmck.spark;

import java.io.FileInputStream;
import java.util.Properties;

import java.io.*;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uchicago.cs.ucare.dmck.server.ModelCheckingServerAbstract;
import edu.uchicago.cs.ucare.dmck.server.SpecVerifier;

public class SparkVerifier extends SpecVerifier {

	protected static final Logger LOG = LoggerFactory.getLogger(SparkVerifier.class);

  	// protected static final Logger LOG = LoggerFactory.getLogger(SCMVerifier.class);

	private Properties kv = new Properties();

	private String errorType = "";
	private String errorLog = "";

	@Override
	public boolean verify() {
		boolean result = true;
		if (!checkFreeFromError()) {
			result = false;

			try{
				Process p  = Runtime.getRuntime().exec("/tmp/spark/stopNodes.sh");
				BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		        String log = "";
		        while ((log = reader.readLine()) != null) {
		            LOG.info(log);
		        }

				LOG.info("Everything is stopped!");
				Runtime.getRuntime().exec("/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-Bug-Exploration/doneAlert.sh");

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return result;
	}

	@Override
	public String verificationDetail() {
		String result = "";
		result += " errorLog: " + errorLog + " ;";
		result += "\n";
		result += errorType;
		errorType = "";
		errorLog = "";
		return result;
	}

	private boolean checkFreeFromError() {
	    final String errorPhrase = "java.nio.channels.ClosedChannelException";

		LOG.info("DMCK checking the Spark logs");

	    int occurrences = 0;
	    int index = 0;

		try {
			File file = new File("/tmp/spark/log/spark-shell.log");
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				if ( line.indexOf(errorPhrase,0) >= 0 ){
					LOG.info("Found the phrase!");
					errorType = "spark-15262";
					// found the phrase
					occurrences++;
					errorLog += "\n" + line;
			        
					LOG.info("====== GOT THE BUG =====");
					
					}
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (occurrences == 0)
			return true;
		else
			return false;
	}

}
