package edu.uchicago.cs.ucare.dmck.spark;

import java.io.*;
import java.util.*;

import edu.uchicago.cs.ucare.dmck.server.WorkloadDriver;

public class SparkWorkloadDriver extends WorkloadDriver {
	private String ipcSparkDir;
	private long millis1 = 0, millis2 = 0; 
	private Thread consoleWriter = new Thread(new LogWriter());;
	private FileOutputStream[] consoleLog;
	Process[] node;

	public SparkWorkloadDriver(int numNode, String workingDir, String ipcDir, String samcDir, String targetSysDir) {
		super(numNode, workingDir, ipcDir, samcDir, targetSysDir);
		LOG.info("Construct SparkWorkloadDriver");
		ipcSparkDir = ipcDir + "-spark";
		node = new Process[numNode];
		consoleLog = new FileOutputStream[numNode];
		// consoleWriter 
		consoleWriter.start();

	}

	@Override
	public void startNode(int id) {

		try {
				// node[id] = Runtime.getRuntime().exec(workingDir + "/startNode.sh " + id + " " + testId +
				// " " + numNode);
				LOG.info("Start Node-" + id);

				Thread.sleep(50);

				if (id == 0){
					// Thread.sleep(1000);
    				millis1 = System.currentTimeMillis();
					LOG.info("Start the Spark-app "+testId+" !");
					
					// LOG.info("Kill CGEB!");
					// Runtime.getRuntime().exec("jps | grep CoarseGrainedExecutorBackend | grep -Eo '[0-9]{1,7}' | while read -r line ; do     echo 'Killing $line';     kill -9 $line; done").waitFor();
					// Runtime.getRuntime().exec("touch /tmp/ipc/manual/cgeb-"+ testId);
					// Runtime.getRuntime().exec("./killCGEB.sh " + testId + " > /tmp/ipc/manual/cgeb-"+ testId);
					
					
					if (testId > 1) {
						// LOG.info("Reset all worker clusters!");
						// Runtime.getRuntime().exec("jps | grep Worker | grep -Eo '[0-9]{1,7}' | while read -r line ; do     echo 'Killing $line';     kill -9 $line; done").waitFor();
						// Runtime.getRuntime().exec(workingDir + "/stopNodes.sh").waitFor();
						// Runtime.getRuntime().exec(workingDir + "/prepareNodes.sh").waitFor();
						// Runtime.getRuntime().exec("/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-Bug-Exploration/DMCK-master/spark/sbin/stop-slaves.sh").waitFor();
						// Runtime.getRuntime().exec("/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-Bug-Exploration/DMCK-master/spark/sbin/start-slaves.sh").waitFor();					
						Runtime.getRuntime().exec("cat /tmp/ipc/manual/mauliat > /tmp/ipc/manual/mauliat-"+testId);	
					}
					
					Runtime.getRuntime().exec("./startTask.sh > /tmp/ipc/manual/mauliat");
					// Runtime.getRuntime().exec("./alertStartTask.sh " + testId);
					
					// Runtime.getRuntime().exec("/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-15262/DMCK-master/spark" + "/startTask.sh " + testId);
					// Runtime.getRuntime().exec("/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-15262/DMCK-master/spark" + "/alertStartTask.sh " + testId);
					
					Thread.sleep(500);
					// Thread.sleep(2000);
					// // Runtime.getRuntime().exec(workingDir + "/startTaskX.sh " + testId++);
					// int nextTestId = testId + 1;
					// // Thread.sleep(1000);
					// LOG.info("Start the Spark-app "+nextTestId+" !");
					// Runtime.getRuntime().exec(workingDir + "/startTask.sh " + nextTestId);
				}

				// if (id == 0 && testId > 1){
				// 	int nextTestId = testId + 1;
				// 	// Thread.sleep(1000);
				// 	LOG.info("Start the Spark-app "+nextTestId+" !");
				// 	Runtime.getRuntime().exec(workingDir + "/startTask.sh " + nextTestId);
				// 	// Thread.sleep(2000);
				// 	// Runtime.getRuntime().exec(workingDir + "/startTaskX.sh " + testId++);
				// }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stopNode(int id) {

		try {
			if (id == 0){
				
				millis2 = System.currentTimeMillis();
				// Process p = Runtime.getRuntime().exec(workingDir + "/alertStopTask.sh " + (millis2-millis1)/1000 );
				Runtime.getRuntime().exec(workingDir + "/killTask.sh ");

				// Process p = Runtime.getRuntime().exec("/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-15262/DMCK-master/spark" + "/alertStopTask.sh " + (millis2-millis1)/1000 );
				// Runtime.getRuntime().exec("/Users/zhorifiandi/Documents/gik/TA/eachscenario/SPARK-15262/DMCK-master/spark" + "/killTask.sh ");


				 // Grab output and print to display
		        // BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		        // String line = "";
		        // while ((line = reader.readLine()) != null) {
		        //     LOG.info(line);
		        // }

				LOG.info("All the node(s) are stopped!");
				Thread.sleep(50);
			}
		} catch (Exception e) {
			LOG.error("Error when DMCK tries to kill node " + id);
		}
	}


	@Override
	public void startWorkload() {
		// try {
		// 	LOG.info("Start Workload in Spark");
		// 	Runtime.getRuntime().exec(workingDir + "/startWorkload-cqlsh.sh " + testId);
		// } catch (Exception e) {
		// 	LOG.error("Error in running startWorkload-cqlsh.sh");
		// }
	}

	@Override
	public void stopWorkload() {
		// try {
		// 	LOG.info("Stop Workload in Spark");
		// 	Runtime.getRuntime().exec(workingDir + "/killWorkload-cqlsh.sh");
		// } catch (Exception e) {
		// 	LOG.error("Error in running killWorkload-cqlsh.sh");
		// }
	}

	class LogWriter implements Runnable {

		public void run() {
			byte[] buff = new byte[256];
			while (true) {
				for (int i = 0; i < numNode; ++i) {
					if (node[i] != null) {
						int r = 0;
						InputStream stdout = node[i].getInputStream();
						InputStream stderr = node[i].getErrorStream();
						try {
							consoleLog = new FileOutputStream[i];
							while ((r = stdout.read(buff)) != -1) {
								consoleLog[i].write(buff, 0, r);
								consoleLog[i].flush();
							}
							while ((r = stderr.read(buff)) != -1) {
								consoleLog[i].write(buff, 0, r);
								consoleLog[i].flush();
							}
						} catch (IOException e) {
							LOG.warn("Error in writing log");
						}
					}
				}
				try {
					Thread.sleep(300);
				} catch (Exception e) {
					LOG.warn("Error in LogWriter thread sleep");
				}
			}
		}

	}
}
