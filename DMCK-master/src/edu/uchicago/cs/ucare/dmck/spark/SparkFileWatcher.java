package edu.uchicago.cs.ucare.dmck.spark;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Properties;
import edu.uchicago.cs.ucare.dmck.event.Event;
import edu.uchicago.cs.ucare.dmck.server.FileWatcher;
import edu.uchicago.cs.ucare.dmck.server.ModelCheckingServerAbstract;

public class SparkFileWatcher extends FileWatcher {

	public SparkFileWatcher(String sPath, ModelCheckingServerAbstract dmck) {
		super(sPath, dmck);
	}

	@Override
	public synchronized void proceedEachFile(String filename, Properties ev) {
		if (filename.startsWith("spark-")) {
			// LOG.info("FileWatcher: proceedEachFile spark-");
			int sendNode = Integer.parseInt(ev.getProperty("sendNode"));
			int recvNode = Integer.parseInt(ev.getProperty("recvNode"));
			long eventId = Long.parseLong(ev.getProperty("eventId"));
			int eventTypeId = Integer.parseInt(ev.getProperty("eventTypeId"));
			String eventType = ev.getProperty("eventType");
			String payload = ev.getProperty("payload");
			// int chainId = Integer.parseInt(ev.getProperty("chainId"));
			// int stageId = Integer.parseInt(ev.getProperty("stageId"));

			long hashId = commonHashId(eventId);

			LOG.info("Receive msg " + filename + ": eventTypeId-"+ eventTypeId+ " : stageId-" + " payload-" + payload );
			appendReceivedUpdates("New Event: filename=" + filename + " payload-" + payload + " sendNode=" + sendNode + " recvNode=" + recvNode
					+ " eventType=" + eventType + " eventId=" + eventId);

			Event event = new Event(hashId);
			event.addKeyValue(Event.FROM_ID, sendNode);
			event.addKeyValue(Event.TO_ID, recvNode);
			event.addKeyValue(Event.FILENAME, filename);
			// event.addKeyValue("chainId", chainId);
			// event.addKeyValue("stageId", stageId);
			event.addKeyValue("eventTypeId", eventTypeId);
			event.addKeyValue("payload", payload);
			event.setVectorClock(dmck.getVectorClock(sendNode, recvNode));
			dmck.offerPacket(event);
		} else if (filename.startsWith("sparkUpdate-")){
			int eventTypeId = Integer.parseInt(ev.getProperty("eventTypeId"));
			int sendNode = Integer.parseInt(ev.getProperty("sendNode"));
			String eventType = ev.getProperty("eventType");
			String payload = ev.getProperty("payload");
			dmck.localStates[sendNode].setKeyValue(eventTypeId + "Update-" + eventType, payload);
		}

		removeProceedFile(filename);
	}
	
  @Override
  protected void sequencerEnablingSignal(Event packet) {
    // Since current DMCK integration with SCM has not supported sequencer yet,
    // DMCK should just use common enabling signal function for now.
    commonEnablingSignal(packet);
  }

}
