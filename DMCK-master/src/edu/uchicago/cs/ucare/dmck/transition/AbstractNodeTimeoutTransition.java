package edu.uchicago.cs.ucare.dmck.transition;

import java.util.LinkedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.uchicago.cs.ucare.dmck.server.ModelCheckingServerAbstract;

@SuppressWarnings("serial")
public class AbstractNodeTimeoutTransition extends AbstractNodeOperationTransition {

  private final static Logger LOG = LoggerFactory.getLogger(AbstractNodeTimeoutTransition.class);

  public AbstractNodeTimeoutTransition(ModelCheckingServerAbstract dmck) {
    super(dmck);
  }

  public AbstractNodeTimeoutTransition(int numNode) {
    super(numNode);
  }

  public AbstractNodeTimeoutTransition(ModelCheckingServerAbstract dmck, boolean isRandom) {
    super(dmck, isRandom);
  }

  @Override
  public boolean apply() {
    NodeTimeoutTransition t = getRealNodeOperationTransition();
    if (t == null) {
      return false;
    }
    id = t.getId();
    return t.apply();
  }

  @Override
  public long getTransitionId() {
    return 101;
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof AbstractNodeTimeoutTransition;
  }

  @Override
  public int hashCode() {
    return 101;
  }

  public NodeTimeoutTransition getRealNodeOperationTransition() {
    if (isRandom) {
      LinkedList<NodeOperationTransition> allPossible =
          getAllRealNodeOperationTransitions(dmck.isNodeOnline);
      if (allPossible.isEmpty()) {
        LOG.debug("Try to execute timeout node event, but currently there is no online node");
        return null;
      }
      int i = RANDOM.nextInt(allPossible.size());
      return (NodeTimeoutTransition) allPossible.get(i);
    } else {
      for (int i = 0; i < dmck.numNode; ++i) {
        if (dmck.isNodeOnline(i)) {
          NodeTimeoutTransition realTimeout = new NodeTimeoutTransition(dmck, i);
          realTimeout.setVectorClock(getPossibleVectorClock(i));
          return realTimeout;
        }
      }
      LOG.debug("Try to execute timeout node event, but currently there is no online node");
      return null;
    }
  }

  @Override
  public NodeOperationTransition getRealNodeOperationTransition(int suggestExecuteNodeId) {
    if (dmck.isNodeOnline(suggestExecuteNodeId)) {
      NodeTimeoutTransition realTimeout = new NodeTimeoutTransition(dmck, suggestExecuteNodeId);
      realTimeout.setVectorClock(getPossibleVectorClock(suggestExecuteNodeId));
      return realTimeout;
    }
    LOG.debug(
        "Try to execute timeout node event based on suggestion, but currently suggested node is not online");
    return null;
  }

  @Override
  public LinkedList<NodeOperationTransition> getAllRealNodeOperationTransitions(
      boolean[] onlineStatus) {
    LinkedList<NodeOperationTransition> result = new LinkedList<NodeOperationTransition>();
    for (int i = 0; i < onlineStatus.length; ++i) {
      if (onlineStatus[i]) {
        NodeTimeoutTransition realTimeout = new NodeTimeoutTransition(dmck, i);
        realTimeout.setVectorClock(getPossibleVectorClock(i));
        result.add(realTimeout);
      }
    }
    return result;
  }

  @Override
  public LinkedList<NodeOperationTransition> getAllRealNodeOperationTransitions() {
    LinkedList<NodeOperationTransition> result = new LinkedList<NodeOperationTransition>();
    for (int i = 0; i < dmck.numNode; ++i) {
      NodeTimeoutTransition realTimeout = new NodeTimeoutTransition(dmck, i);
      realTimeout.setVectorClock(getPossibleVectorClock(i));
      result.add(realTimeout);
    }
    return result;
  }

  public String toString() {
    return "abstract_timeout";
  }

  @Override
  public String toStringForFutureExecution() {
    return "abstract_timeout";
  }

}
